CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module only create required fields to brazilian payment providers,
specially to Commerce Pagseguro Transparente. Visit:
https://www.drupal.org/project/commerce_pagseguro_transp.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/br_billing_fields

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/br_billing_fields

REQUIREMENTS
------------

This module requires the following modules:

 * cpf (https://drupal.org/project/cpf)
 * telephone (https://drupal.org/project/telephone)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, you must go to Configurations -> People -> Account settings and Manage
Display, Manage form display to show the newly created fields.

MAINTAINERS
-----------

Current maintainers:
 * Bruno de Oliveira Magalhaes (bmagalhaes) - https://drupal.org/user/333078

This project has been sponsored by:
 * 7Links Web Solutions
   Specialized in consulting and planning of Drupal powered sites, 7Links
   offers installation, development, theming, customization, and hosting
   to get you started. Visit https://7links.com.br for more information.
